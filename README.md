# Digit Recognizer on MNIST dataset

This paper presents a computer vision algorithm, based on the implementation of a convolution neural network (CNN),
as part of the image classification of the MNIST dataset. 
This work focused on the virtual environment proposed by the Kaggle platform, which hosts the competition :
"_Learn computer vision fundamentals with the famous MNIST data_"

## Project Score

This approach resulted in a public score of __0.99971__, corresponding to the percentage of correct predictions.